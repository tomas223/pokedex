import React, { DOMAttributes, ReactNode } from "react";
import c from "classnames";
import styles from "./Button.module.css";

type Props = {
  primary?: boolean;
  children?: ReactNode;
  onClick?: DOMAttributes<HTMLDivElement>["onClick"];
};

function Button(props: Props) {
  const { primary, children, onClick } = props;

  const className = c(
    "pure-button",
    styles.button,
    primary && styles.buttonPrimary
  );

  return (
    <div onClick={onClick} className={className}>
      {children}
    </div>
  );
}

export default Button;
