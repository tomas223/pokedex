import React from "react";
import c from "classnames";
import { GridLoader } from "react-spinners";

import styles from "./LoadingSpinner.module.css";
import { colorPrimary } from "@/styles/variables";

type Props = {
  loading?: boolean;
  center?: boolean;
  margin?: boolean;
};

function LoadingSpinner(props: Props) {
  const { loading = true, center, margin } = props;
  return (
    <div className={c(margin && styles.margin, center && styles.center)}>
      <GridLoader loading={loading} color={colorPrimary} />
    </div>
  );
}

export default LoadingSpinner;
