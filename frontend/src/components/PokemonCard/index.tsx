import React, { ReactNode } from "react";
import Image from "next/image";
import { AiFillHeart, AiOutlineHeart } from "react-icons/ai";

import { Pokemon } from "@/__generated__/graphql";
import styles from "./PokemonCard.module.css";
import Link from "next/link";
import IconButton from "../IconButton";
import FavoriteButton from "@/containers/FavoriteButton";

export type IPokemonCard = Pick<Pokemon, "name" | "image" | "id"> & {
  isFavorite: boolean;
  showDetail?: boolean;
  types?: string[];
};

const iconProps = {
  color: "red",
  size: "1.5em",
};

function PokemonCard(props: IPokemonCard) {
  const { name, image, types, isFavorite, showDetail, id } = props;
  return (
    <div className={styles.card}>
      <Link href={`/detail/${name}`}>
        <div className={styles.cardImageWrap}>
          <Image src={image} alt={name} width="300" height="100" />
        </div>
      </Link>

      <div className={styles.cardDescription}>
        <div className={styles.cardDescriptionTexts}>
          <h3>{name}</h3>
          {showDetail && types && <p>{types.join(", ")}</p>}
        </div>

        <div style={{ display: "flex", alignItems: "center" }}>
          <FavoriteButton isFavorite={isFavorite} id={id} />
        </div>
      </div>
    </div>
  );
}

export default PokemonCard;
