import React, { InputHTMLAttributes, ReactNode } from "react";
// import c from "classnames";
// import styles from "./Form.module.css";

type Value = InputHTMLAttributes<HTMLInputElement>["value"];

type Props = Pick<
  InputHTMLAttributes<HTMLInputElement>,
  "value" | "placeholder"
> & {
  onValueChange?: (value: string) => void;
  className?: string;
  children?: ReactNode;
  // select?: boolean;
};

function Input(props: Props) {
  const { className, onValueChange, ...rest } = props;

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) =>
    onValueChange?.(e.target.value);

  return (
    <input
      onChange={onChange}
      // className={c(styles.inputCommon, className)}
      className={className}
      {...rest}
    />
  );
}

export default Input;
