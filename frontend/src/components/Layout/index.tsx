import { ReactNode } from "react";
import Head from "next/head";
import styles from "./Layout.module.css";

type Props = {
  pageTitle?: string;
  children?: ReactNode;
};

export default function Layout(props: Props) {
  const { pageTitle, children } = props;

  const title = `Pokedex${pageTitle ? " | " + pageTitle : ""}`;
  return (
    <div className={styles.container}>
      <Head>
        <title>{title}</title>
        <meta name="description" content="Pokedex in Browser" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1"
          key="viewport"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>{children}</main>
    </div>
  );
}
