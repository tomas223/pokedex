import FavoriteButton from "@/containers/FavoriteButton";
import React, { ReactNode } from "react";
import Grid from "../Grid";
import PokemonCard, { IPokemonCard } from "../PokemonCard";

const cardGridClass = (gridEnabled: boolean) =>
  gridEnabled
    ? "pure-u-1-1 pure-u-sm-1-2 pure-u-md-1-3 pure-u-lg-1-4"
    : "pure-u-1-1";

type Props = {
  pokeList: IPokemonCard[] | undefined;
  showDetail?: boolean;
  // onFavouriteClick?: (pokemonId: string) => void;
  children?: ReactNode;
  displayInGrid?: boolean;
};

function PokemonList(props: Props) {
  const {
    pokeList,
    showDetail = true,
    // onFavouriteClick,
    children,
    displayInGrid = true,
  } = props;
  return (
    <Grid container style={{ marginTop: "0.5rem" }}>
      {pokeList?.map((pkmn) => (
        <div className={cardGridClass(displayInGrid)} key={pkmn.id}>
          <PokemonCard
            showDetail={showDetail}
            // favoriteButton={
            //   <FavoriteButton
            //     isFavorite={data.pokemonByName.isFavorite}
            //     id={data.pokemonByName.id}
            //   />
            // }
            // onFavouriteClick={onFavouriteClick}
            {...pkmn}
          />
        </div>
      ))}
      {children}
    </Grid>
  );
}

export default PokemonList;
