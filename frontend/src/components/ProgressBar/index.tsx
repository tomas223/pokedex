import { colorPrimary } from "@/styles/variables";
import React from "react";

type Props = { color?: string };

function ProgressBar(props: Props) {
  const { color = colorPrimary } = props;

  return (
    <div
      style={{
        height: "0.5em",
        backgroundColor: color,
        flex: 1,
        borderRadius: "0.25em",
      }}
    />
  );
}

export default ProgressBar;
