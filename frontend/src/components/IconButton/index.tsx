import React, { DOMAttributes, HTMLAttributes, ReactNode } from "react";
import styles from "./IconButton.module.css";

type Props = {
  children?: ReactNode;
  onClick?: DOMAttributes<HTMLDivElement>["onClick"];
  role?: HTMLAttributes<HTMLDivElement>["role"];
  style?: HTMLAttributes<HTMLDivElement>["style"];
  // hidden?: HTMLAttributes<HTMLDivElement>["hidden"];
};

function IconButton(props: Props) {
  const { children, onClick, role = "button", style } = props;

  return (
    <div
      // hidden={hidden}
      style={style}
      onClick={onClick}
      role={role}
      className={styles.container}
    >
      {children}
    </div>
  );
}

export default IconButton;
