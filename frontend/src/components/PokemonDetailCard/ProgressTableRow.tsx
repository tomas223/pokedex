import React from "react";
import ProgressBar from "../ProgressBar";

type Props = { text?: string; color?: string };

function ProgressTableRow(props: Props) {
  const { text, color } = props;

  return (
    <tr>
      <td style={{ width: "100%" }}>
        <ProgressBar color={color} />
      </td>
      <td>
        <h5 style={{ whiteSpace: "pre", margin: "0.5em 0 0.5em 0.5em" }}>
          {text}
        </h5>
      </td>
    </tr>
  );
}

export default ProgressTableRow;
