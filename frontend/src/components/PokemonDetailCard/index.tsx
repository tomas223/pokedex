import Image from "next/image";
import React, { ReactNode, useEffect, useState } from "react";
import { HiSpeakerWave } from "react-icons/hi2";

import IconButton from "../IconButton";
import styles from "./PokemonDetailCard.module.css";
import { Pokemon } from "@/__generated__/graphql";
import { colorPrimary, colorSecondary } from "@/styles/variables";
import ProgressTableRow from "./ProgressTableRow";
import FavoriteButton from "@/containers/FavoriteButton";

type Props = {
  pokemon: Pick<
    Pokemon,
    | "name"
    | "maxCP"
    | "maxHP"
    | "types"
    | "image"
    | "sound"
    | "weight"
    | "height"
    | "isFavorite"
    | "id"
  >;
};

const SPEAKER_ICON_SIZE = "1.8em";

function PokemonDetailCard(props: Props) {
  const { pokemon } = props;
  const {
    name,
    maxCP,
    maxHP,
    types,
    image,
    sound,
    weight,
    height,
    isFavorite,
    id,
  } = pokemon;
  const [audio, setAudio] = useState(new Audio(sound));

  useEffect(() => setAudio(new Audio(sound)), [sound]);
  const playSound = () => audio.play();

  return (
    <div className={styles.container}>
      <div className={styles.imageContainer}>
        <IconButton onClick={playSound}>
          <HiSpeakerWave size={SPEAKER_ICON_SIZE} color={colorPrimary} />
        </IconButton>
        <div>
          <Image
            className={styles.image}
            src={image || "/placeholder.png"}
            alt={name || "pokemon"}
            width="900"
            height="900"
          />
        </div>

        <IconButton style={{ visibility: "hidden" }} role="">
          <HiSpeakerWave size={SPEAKER_ICON_SIZE} />
        </IconButton>
      </div>

      <div className={styles.detailInfo}>
        <div className={styles.detailInfoTop}>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <div>
              <h2>{name}</h2>
              <p>{types?.join(", ")}</p>
            </div>

            <FavoriteButton isFavorite={isFavorite} id={id} />
          </div>

          <table style={{ width: "100%" }}>
            <tbody>
              <ProgressTableRow text={`CP: ${maxCP}`} />
              <ProgressTableRow text={`HP: ${maxHP}`} color={colorSecondary} />
            </tbody>
          </table>
        </div>

        <div className={styles.detailInfoBottom}>
          <div>
            <h4>Weight</h4>
            <p>
              {weight.minimum} - {weight.maximum}
            </p>
          </div>
          <div>
            <h4>Height</h4>
            <p>
              {height.minimum} - {height.maximum}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PokemonDetailCard;
