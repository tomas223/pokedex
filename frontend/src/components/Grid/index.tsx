import React, { CSSProperties, ReactNode } from "react";
import classes from "./Grid.module.css";
import c from "classnames";

type Props = {
  className?: string;
  container: true; // force this for future use also as grid "unit"
  children?: ReactNode;
  style?: CSSProperties;
};

function Grid(props: Props) {
  const { className, children, style } = props;

  return (
    <div style={style} className={c(classes.container, className)}>
      {children}
    </div>
  );
}

export default Grid;
