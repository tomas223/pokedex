import { ListPokemonsQuery } from "@/__generated__/graphql";

const pokemonFetchMoreMerge = (
  prev: ListPokemonsQuery,
  current: ListPokemonsQuery
) => {
  const merged = {
    pokemons: {
      ...prev.pokemons,
      edges: [...prev.pokemons.edges, ...current.pokemons.edges],
    },
  };
  return merged;
};

export { pokemonFetchMoreMerge };
