import {
  ApolloClient,
  InMemoryCache,
  useQuery as apolloUseQuery,
} from "@apollo/client";
import { offsetLimitPagination } from "@apollo/client/utilities";

import { gql as typedGql } from "@/__generated__/gql";

const hostUri = "http://localhost:4000/graphql";

const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        "pokemons.edges": offsetLimitPagination(),
      },
    },
  },
});

const client = new ApolloClient({
  uri: hostUri,
  cache,
});

const useQuery = apolloUseQuery;
const gql = typedGql;

export { client, gql, useQuery };
