import { gql } from "@/__generated__/gql";

export const LIST_POKEMONS = gql(/* GraphQL */ `
  query ListPokemons($query: PokemonsQueryInput!) {
    pokemons(query: $query) {
      edges {
        id
        name
        image
        types
        isFavorite
      }
    }
  }
`);

export const LIST_POKEMON_TYPES = gql(/* GraphQL */ `
  query ListPokemonTypes {
    pokemonTypes
  }
`);

export const POKEMON_DETAIL_BY_NAME = gql(/* GraphQL */ `
  query PokemonDetailByName($name: String!) {
    pokemonByName(name: $name) {
      id
      isFavorite
      name
      types
      image
      maxHP
      maxCP
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
      sound
      evolutions {
        id
        name
        image
        isFavorite
      }
    }
  }
`);

export const MUTATE_FAVORITE_POKEMON = gql(/* GraphQL */ `
  mutation FavoritePokemon($id: ID!) {
    favoritePokemon(id: $id) {
      id
      name
      image
      types
      isFavorite
    }
  }
`);

export const MUTATE_UNFAVORITE_POKEMON = gql(/* GraphQL */ `
  mutation UnFavoritePokemon($id: ID!) {
    unFavoritePokemon(id: $id) {
      id
      name
      image
      types
      isFavorite
    }
  }
`);
