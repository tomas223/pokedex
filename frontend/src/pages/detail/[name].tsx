import Layout from "@/components/Layout";
import DetailView from "@/views/DetailView";
import Link from "next/link";
import { useRouter } from "next/router";

export default function Detail() {
  const router = useRouter();

  const pokemonName = router.query.name;

  return (
    <Layout pageTitle="Detail">
      {typeof pokemonName === "string" ? (
        <DetailView pokemonName={pokemonName} />
      ) : (
        <h1>
          <Link href="/">404</Link>
        </h1>
      )}
    </Layout>
  );
}
