import Layout from "@/components/Layout";
import ListView from "@/views/ListView";

export default function Home() {
  return (
    <Layout pageTitle="Home">
      <ListView />
    </Layout>
  );
}
