export const colorPrimary = "#71c1a1";
export const colorSecondary = "#9F9EFF";

export const colorGrayDarker = "#dddddd";
