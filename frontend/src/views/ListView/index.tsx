import React, { useState } from "react";
import { useDebounce } from "usehooks-ts";

import { LIST_POKEMONS, LIST_POKEMON_TYPES } from "@/services/api/queries";
import ListViewHeader from "./ListViewHeader";
import { useQuery } from "@/services/api";
import { Grid, LoadingSpinner, PokemonCard, PokemonList } from "@/components";
// import styles from "./ListView.module.css";
import { InView } from "react-intersection-observer";
import { pokemonFetchMoreMerge } from "@/services/api/misc";
// import useKeyPress from "@/utils/useKeyPress";

const RESULT_NUM_BASE = 20;

function ListView() {
  const [sQuery, setSQuery] = useState<string>("");
  const [sType, setSType] = useState<string>("");
  const debouncedSearchQuery = useDebounce(sQuery, 300);
  const [favoriteOnly, setFavoriteOnly] = useState(false);
  const [displayInGrid, setDisplayInGrid] = useState(true);

  const { data, loading, fetchMore } = useQuery(LIST_POKEMONS, {
    variables: {
      query: {
        offset: 0,
        limit: RESULT_NUM_BASE,
        search: debouncedSearchQuery,
        filter: { type: sType, isFavorite: favoriteOnly },
      },
    },
    notifyOnNetworkStatusChange: true,
  });

  const { data: pokemonTypes } = useQuery(LIST_POKEMON_TYPES);

  return (
    <>
      <ListViewHeader
        favorite={favoriteOnly}
        onFavoriteChange={(val) => setFavoriteOnly(val)}
        onSearchChange={setSQuery}
        search={sQuery}
        selectOptions={pokemonTypes?.pokemonTypes}
        onSelectChange={setSType}
        selectValue={sType}
        displayInGrid={displayInGrid}
        onDisplayInGridChange={setDisplayInGrid}
      />

      <PokemonList
        pokeList={data?.pokemons.edges}
        displayInGrid={displayInGrid}
      >
        {data && (
          <InView
            onChange={async (inView) => {
              const currentLength = data.pokemons.edges?.length || 0;
              if (inView) {
                fetchMore({
                  variables: {
                    query: {
                      offset: currentLength,
                      limit: currentLength + RESULT_NUM_BASE,
                      search: debouncedSearchQuery,
                      filter: { type: sType, isFavorite: favoriteOnly },
                    },
                  },
                  updateQuery: (prev, { fetchMoreResult }) =>
                    pokemonFetchMoreMerge(prev, fetchMoreResult),
                });
              }
            }}
          />
        )}
      </PokemonList>

      <LoadingSpinner center margin loading={loading} />
    </>
  );
}

export default ListView;
