import React from "react";
import c from "classnames";
import { BiGridHorizontal } from "react-icons/bi";
import { BsList } from "react-icons/bs";

import { Button, Grid, IconButton, Input } from "@/components";
import styles from "./ListView.module.css";
import { colorGrayDarker, colorPrimary } from "@/styles/variables";

type Props = {
  search: string;
  onSearchChange: (value: string) => void;
  selectOptions: string[] | undefined;
  selectValue: string;
  onSelectChange: (value: string) => void;
  favorite: boolean;
  onFavoriteChange: (fav: boolean) => void;
  displayInGrid: boolean;
  onDisplayInGridChange: (inGrid: boolean) => void;
};

function ListViewHeader(props: Props) {
  const {
    search,
    onSearchChange,
    selectOptions,
    selectValue,
    onSelectChange,
    favorite,
    onFavoriteChange,
    displayInGrid,
    onDisplayInGridChange,
  } = props;
  return (
    <div className={styles.header}>
      <div className={styles.headerTop}>
        <Button onClick={() => onFavoriteChange(false)} primary={!favorite}>
          All
        </Button>
        <Button onClick={() => onFavoriteChange(true)} primary={favorite}>
          Favorites
        </Button>
      </div>
      <Grid container>
        <div className="pure-u-1 pure-u-sm-3-5">
          <Input
            value={search}
            onValueChange={onSearchChange}
            placeholder="Search"
          />
        </div>
        <div className={c("pure-u-1", "pure-u-sm-2-5", styles.typeGridItem)}>
          <select
            onChange={(e) => onSelectChange(e.target.value)}
            value={selectValue}
          >
            <option value="">All</option>
            {selectOptions?.map((option) => (
              <option value={option} key={option}>
                {option}
              </option>
            ))}
          </select>
          <IconButton
            onClick={() => onDisplayInGridChange(true)}
            style={{ marginLeft: "0.5em" }}
          >
            <BiGridHorizontal
              color={displayInGrid ? colorPrimary : "#000"}
              size="1.5em"
            />
          </IconButton>
          <IconButton onClick={() => onDisplayInGridChange(false)}>
            <BsList
              color={!displayInGrid ? colorPrimary : "#000"}
              size="1.5em"
            />
          </IconButton>
        </div>
      </Grid>
    </div>
  );
}

export default ListViewHeader;
