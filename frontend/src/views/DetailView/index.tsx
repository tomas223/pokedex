import React from "react";

import { useQuery } from "@/services/api";
import { POKEMON_DETAIL_BY_NAME } from "@/services/api/queries";
import PokemonList from "@/components/PokemonList";
import { LoadingSpinner, PokemonDetailCard } from "@/components";
import FavoriteButton from "@/containers/FavoriteButton";

type Props = {
  pokemonName: string;
};

function DetailView({ pokemonName }: Props) {
  const { data, loading } = useQuery(POKEMON_DETAIL_BY_NAME, {
    variables: { name: pokemonName },
  });

  const { evolutions } = data?.pokemonByName || {};

  if (!data) return <LoadingSpinner margin center />;

  return (
    <>
      {data?.pokemonByName ? (
        <PokemonDetailCard pokemon={data?.pokemonByName} />
      ) : (
        <h3>Pokemon: {pokemonName} not found</h3>
      )}

      <PokemonList showDetail pokeList={evolutions} />
    </>
  );
}

export default DetailView;
