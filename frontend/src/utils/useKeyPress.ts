import { useState, useEffect, useCallback, KeyboardEvent } from "react";

// type HandlerProps = (e: KeyboardEvent<Element>) => void;

type HandleProps = {
  key: string;
  preventDefault: () => void;
  keyCode: number;
};

function useKeyPress(
  targetKey: string,
  onKeyPress: () => void = () => {}
): boolean {
  const [keyPressed, setKeyPressed] = useState(false);

  const downHandler = useCallback(
    ({ key }: HandleProps) => {
      if (key === targetKey) {
        setKeyPressed(true);
      }
    },
    [targetKey]
  );

  const upHandler = useCallback(
    ({ key }: HandleProps) => {
      if (key === targetKey) {
        setKeyPressed(false);
      }
    },
    [targetKey]
  );

  const pressHandler = useCallback(
    ({ key, preventDefault, keyCode }: HandleProps) => {
      if (key === targetKey) {
        preventDefault();
        setKeyPressed(false);
      }
    },
    [targetKey]
  );

  // Add event listeners
  useEffect(() => {
    window.addEventListener("keydown", downHandler);
    window.addEventListener("keyup", upHandler);
    window.addEventListener("keypress", pressHandler);
    // Remove event listeners on cleanup
    return () => {
      window.removeEventListener("keydown", downHandler);
      window.removeEventListener("keyup", upHandler);
      window.removeEventListener("keypress", pressHandler);
    };
  }, [downHandler, upHandler, onKeyPress, pressHandler]); // Empty array ensures that effect is only run on mount and unmount
  return keyPressed;
}

export default useKeyPress;
