import React from "react";
import { AiFillHeart, AiOutlineHeart } from "react-icons/ai";

import { IconButton } from "@/components";
import { useMutation } from "@apollo/client";
import {
  MUTATE_FAVORITE_POKEMON,
  MUTATE_UNFAVORITE_POKEMON,
} from "@/services/api/queries";

const iconProps = {
  color: "red",
  size: "1.5em",
};

type Props = {
  isFavorite: boolean;
  id: string;
};

function FavoriteButton(props: Props) {
  const { isFavorite, id } = props;
  console.log(isFavorite, id);

  const [mutate, { data, loading }] = useMutation(
    isFavorite ? MUTATE_UNFAVORITE_POKEMON : MUTATE_FAVORITE_POKEMON,
    {
      variables: { id },
      onCompleted(data, clientOptions) {
        console.log("Action done", data);
      },
    }
  );

  return (
    <IconButton
      onClick={() => mutate()}
      //  onClick={() => onFavouriteClick?.(id)}
    >
      {isFavorite ? (
        <AiFillHeart {...iconProps} />
      ) : (
        <AiOutlineHeart {...iconProps} />
      )}
    </IconButton>
  );
}

export default FavoriteButton;
