/* eslint-disable */
import * as types from './graphql';
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel-plugin for production.
 */
const documents = {
    "\n  query ListPokemons($query: PokemonsQueryInput!) {\n    pokemons(query: $query) {\n      edges {\n        id\n        name\n        image\n        types\n        isFavorite\n      }\n    }\n  }\n": types.ListPokemonsDocument,
    "\n  query ListPokemonTypes {\n    pokemonTypes\n  }\n": types.ListPokemonTypesDocument,
    "\n  query PokemonDetailByName($name: String!) {\n    pokemonByName(name: $name) {\n      id\n      isFavorite\n      name\n      types\n      image\n      maxHP\n      maxCP\n      weight {\n        minimum\n        maximum\n      }\n      height {\n        minimum\n        maximum\n      }\n      sound\n      evolutions {\n        id\n        name\n        image\n        isFavorite\n      }\n    }\n  }\n": types.PokemonDetailByNameDocument,
    "\n  mutation FavoritePokemon($id: ID!) {\n    favoritePokemon(id: $id) {\n      id\n      name\n      image\n      types\n      isFavorite\n    }\n  }\n": types.FavoritePokemonDocument,
    "\n  mutation UnFavoritePokemon($id: ID!) {\n    unFavoritePokemon(id: $id) {\n      id\n      name\n      image\n      types\n      isFavorite\n    }\n  }\n": types.UnFavoritePokemonDocument,
};

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query ListPokemons($query: PokemonsQueryInput!) {\n    pokemons(query: $query) {\n      edges {\n        id\n        name\n        image\n        types\n        isFavorite\n      }\n    }\n  }\n"): (typeof documents)["\n  query ListPokemons($query: PokemonsQueryInput!) {\n    pokemons(query: $query) {\n      edges {\n        id\n        name\n        image\n        types\n        isFavorite\n      }\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query ListPokemonTypes {\n    pokemonTypes\n  }\n"): (typeof documents)["\n  query ListPokemonTypes {\n    pokemonTypes\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  query PokemonDetailByName($name: String!) {\n    pokemonByName(name: $name) {\n      id\n      isFavorite\n      name\n      types\n      image\n      maxHP\n      maxCP\n      weight {\n        minimum\n        maximum\n      }\n      height {\n        minimum\n        maximum\n      }\n      sound\n      evolutions {\n        id\n        name\n        image\n        isFavorite\n      }\n    }\n  }\n"): (typeof documents)["\n  query PokemonDetailByName($name: String!) {\n    pokemonByName(name: $name) {\n      id\n      isFavorite\n      name\n      types\n      image\n      maxHP\n      maxCP\n      weight {\n        minimum\n        maximum\n      }\n      height {\n        minimum\n        maximum\n      }\n      sound\n      evolutions {\n        id\n        name\n        image\n        isFavorite\n      }\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  mutation FavoritePokemon($id: ID!) {\n    favoritePokemon(id: $id) {\n      id\n      name\n      image\n      types\n      isFavorite\n    }\n  }\n"): (typeof documents)["\n  mutation FavoritePokemon($id: ID!) {\n    favoritePokemon(id: $id) {\n      id\n      name\n      image\n      types\n      isFavorite\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(source: "\n  mutation UnFavoritePokemon($id: ID!) {\n    unFavoritePokemon(id: $id) {\n      id\n      name\n      image\n      types\n      isFavorite\n    }\n  }\n"): (typeof documents)["\n  mutation UnFavoritePokemon($id: ID!) {\n    unFavoritePokemon(id: $id) {\n      id\n      name\n      image\n      types\n      isFavorite\n    }\n  }\n"];

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = gql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
**/
export function gql(source: string): unknown;

export function gql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> = TDocumentNode extends DocumentNode<  infer TType,  any>  ? TType  : never;